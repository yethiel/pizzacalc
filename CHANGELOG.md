# 2019-05-14
- added a keyboard hint
- added a warning for non-normal-sized gluten-free pizzas

# 2019-02-02
- different method of rounding the price (some prices were off by one cent, thanks to Doug for reporting)

# 2019-01-27
- add changelog link
- stylesheet tweaks

# 2019-01-26
- Improve truncation of the total price
- cleanup